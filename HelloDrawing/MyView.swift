//
//  MyView.swift
//  HelloDrawing
//
//  Created by Mamidi,Sai Prakash Reddy on 4/9/19.
//  Copyright © 2019 Mamidi,Sai Prakash Reddy. All rights reserved.
//

import UIKit

class MyView: UIView {

    var lines: [[CGPoint]] = []
    var colors :[UIColor] = []
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
      lines.append( [(touch?.location(in: self))!] )
        colors.append(UIColor(displayP3Red: CGFloat.random(in: 0 ... 1.0), green: CGFloat.random(in: 0 ... 1.0), blue: CGFloat.random(in: 0 ... 1.0), alpha: 1.0))
        print("Began: \(String(describing: touch?.location(in: self))))")
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        lines[lines.count-1].append((touch?.location(in: self))!)
        print("Ended: \(String(describing: touch?.location(in: self))))")
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        lines[lines.count-1].append((touch?.location(in: self))!)
        setNeedsDisplay()
        print("Moved: \(String(describing: touch?.location(in: self))))")
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        print("Cancelled: \(String(describing: touch?.location(in: self))))")
    }
    override func draw(_ rect: CGRect) {
        // Drawing code
        if lines.count == 0{
            return
        }
        
        //        path.move(to: CGPoint(x: 200, y: 100))
        //        path.addLine(to: CGPoint(x: 300, y: 250))
        //        path.addLine(to: CGPoint(x: 200, y: 400))
        //        path.close()
        //        UIColor.red.set()
        //        path.stroke()
        
        for i in 0 ..< lines.count{
            let path = UIBezierPath()
            path.move(to: lines[i][0])
            for pt in lines[i]{
                path.addLine(to: pt)
                
            }
            colors[i].setStroke()
            path.lineWidth = 5.0
            path.stroke()
        }
    }

    required init?(coder aDecoder: NSCoder) {  // called when RainbowView is in storyboard
        super.init(coder:aDecoder)
        // Instantiating and configuring a UITapGestureRecognizer programmatically
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(tapper(_:)))
        tapGR.numberOfTapsRequired = 2        // Only triggered by a triple (!) tap
        self.addGestureRecognizer(tapGR)     // Associate tapGR with this view
    }
    @objc func tapper(_ sender:UITapGestureRecognizer) -> Void{
       
        lines = []
        colors = []
        setNeedsDisplay()
    
    }
   
    

}

